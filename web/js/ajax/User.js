

// $(function () {
//     $.getScript( "Helper.js", function( data, textStatus, jqxhr ) {
//         console.log( data ); // Data returned
//         console.log( textStatus ); // Success
//         console.log( jqxhr.status ); // 200
//         console.log( "Load was performed." );
//     });
// });


function getAllUsers(limit, offset, success, error) {

    var obj = {
        "method": "getAllUsers",
        "limit" : limit,
        "offset" : offset
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function getUserById(id, success, error) {

    var obj = {
        "method": "getUserById",
        "id" : id,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function deleteUserById(id, success, error) {

    var obj = {
        "method": "deleteUserById",
        "id" : id,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function deleteUserByUserName(usename, success, error) {

    var obj = {
        "method": "deleteUserByUserName",
        "username" : usename,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}




function addUser(user, success, error) {

    var date = currentUnixTime();

    console.log(date);

    assertNotEmptyString(user.username);
    assertNotEmptyString(user.fname);
    assertNotEmptyString(user.lname);
    assertNotEmptyString(user.passwd);
    assertNotEmptyString(user.email);
    assertNotEmptyString(user.gender);
    assertNotEmptyString(user.icon);
    assertNotEmptyString(user.dob);
    assertNotEmptyString(user.country);
    assertNotEmptyString(user.description);

    user.method = "addUser";

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        data: JSON.stringify(user),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function modifyUser(user, success, error) {

    assertNotEmptyString(user.id);
    assertNotEmptyString(user.fname);
    assertNotEmptyString(user.passwd);
    assertNotEmptyString(user.email);
    assertNotEmptyString(user.gender);
    assertNotEmptyString(user.icon);
    assertNotEmptyString(user.dob);
    assertNotEmptyString(user.country);
    assertNotEmptyString(user.description);

    user.method = "modifyUser";

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        data: JSON.stringify(user),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function resetUserPasswd(user_id, newPwd, success, error) {

    var obj = {
        method : "resetUserPasswd",
        user_id : user_id,
        new_pwd : newPwd,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });

}

function updateUserSession(success, error) {


    var obj = {
        method : "updateUserSession",
    }

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UpdateUserSessionServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function getUserByUserName(userName, success, error) {

    var obj = {
        "method": "getUserByUserName",
        "userName" : userName,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function getAvatarList(userId, success, error) {
    var obj = {
        "method": "getAvatarList",
        "user_id" : userId,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'UserServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}