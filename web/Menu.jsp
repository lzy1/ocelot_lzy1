<%@ page import="org.jooq.util.derby.sys.Sys" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
    <meta charset="UTF-8">
    <title>Ocelot - Web Blog</title>
    <link rel="icon" type="image/jpg" href="Photos/ocelot.jpg"/>

    <link href="Login.css" rel="stylesheet" type="text/css">
    <link href="preloader.css" rel="stylesheet" type="text/css">
    <link href="css/vendor/imagepicker.css" rel="stylesheet" type="text/css">

    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="js/Helper.js"></script>
    <script src="js/ajax/User.js"></script>
    <script src="js/ajax/Article.js"></script>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Include Bootstrap Datepicker -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <!-- Google reCAPTCHA -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <!-- Summernore -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <!-- JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>

    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/bootstrap.min.css"/>

    <!-- include Markcss -->
    <link rel="stylesheet" type="text/css" href="Menu.css">



    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul id="navbar" class="nav navbar-nav">

                    <li> <a href="Menu.jsp"><span class="glyphicon glyphicon-home"></span>Main Page</a></li>
                    <li id="myPage"><a href="Profile.jsp"><span class="glyphicon glyphicon-user"></span>My Page</a></li>
                    <li id="dropdown" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Search By<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="showTitleSearchBar()">Search By Article Title</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" onclick="showUsernameSearchBar()">Search By Username</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" onclick="showDateSearchBar()">Search By date</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-left">
                    <div class="form-group">
                        <input id="searchTitleInput" type="text" class="form-control" placeholder="Search By Article">
                        <input id="searchUsernameInput" type="text" class="form-control" placeholder="Search By Username">

                        <div class="input-group input-append date" id="datePickerSearchFrom">
                            <input id="searchDateFromInput" type="text" class="form-control" placeholder="Search By Date (From)">
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>

                        <div class="input-group input-append date" id="datePickerSearchTo">
                            <input id="searchDateToInput" type="text" class="form-control" placeholder="Search By Date (To)">
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>

                    </div>
                    <button type="button" onclick="search()" class="btn btn-default">Search</button>
                </form>
                <div class="collapse navbar-collapse btn-group navbar-right">
                    <span id="loginLogout"></span>
                </div>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <script>


        var BATCH_SIZE = 6;
        var loadFunction = loadLatestArticles;


            /** Search Bar Control  **/
            $('#searchUsernameInput').hide();
            $('#searchDateFromInput').hide();
            $('#searchDateToInput').hide();
            $('#datePickerSearchFrom').hide();
            $('#datePickerSearchTo').hide();

        function insertArticleElement(parentRow, article) {

            var elementCol = $("<div></div>");
            parentRow.append(elementCol);

            elementCol.attr("class", "col-sm-6 col-md-4");
            elementCol.attr("id", "articleCol" + article.id);

            var elementPanel = $("<div></div>");
            elementCol.append(elementPanel);
            elementPanel.attr("class", "panel panel-default");
            elementPanel.attr("id", "articlePanel" + article.id);

            var elementTitle = $("<div></div>");
            elementPanel.append(elementTitle);
            elementTitle.attr("class", "panel panel-heading");
            elementTitle.attr("id", "articleTitle" + article.id);

            var link = $("<a></a>");
            link.attr("href", "ViewSingleArticleServlet?article_id=" + article.id);
            link.attr("crossPageArticleId", article.id);


            var h3Title = $("<h3></h3>");
            h3Title.attr("class", "panel-title");
            h3Title.text(article.title);
            link.append(h3Title);

            elementTitle.append(link);

            var elementArticleBody = $("<div></div>");
            elementPanel.append(elementArticleBody);
            elementArticleBody.attr("class", "panel-body");
            elementArticleBody.attr("id", "articleContent" + article.id);

            var elementImgDiv = $("<div></div>");
            elementImgDiv.attr("class", "col-sm-3");

            var elementImg = $("<img>");
            elementImgDiv.append(elementImg);
            elementArticleBody.append(elementImgDiv);
            elementImg.attr("class", "testing");
            elementImg.attr("class", "img-circle");
            elementImg.attr("height", "65");
            elementImg.attr("width", "65");
            elementImg.attr("src", article.userIcon);

            var contentPDiv = $("<div></div>");
            contentPDiv.attr("class", "col-sm-9");

            var contentP = $("<p></p>");


            var previewArticle = article.content.substring(0,50);

            if (previewArticle.length != 50){
                for (var j; j<previewArticle.length; j++){
                    var space = " ";
                    previewArticle += space;
                }

            }

            if (article.content.length <50){
                contentP.append(previewArticle);
            } else{
                contentP.append(previewArticle + "...");
            }
            contentPDiv.append(contentP);
            elementArticleBody.append(contentPDiv);


        }

        function findLastArticleRow() {

            var row = $("#articleRows > div").last();
            if (row.length === 0) {
                row = $("<div></div>");
                row.attr("class", "row flex");
                $("#articleRows").append(row);
                return row;
            } else {
                if (row[0].childElementCount >= 3) {
                    row = $("<div></div>");
                    row.attr("class", "row flex");
                    $("#articleRows").append(row);
                    return row;
                } else {
                    return row;
                }
            }
        }

        function displayArticles(articles, hasMore) {

            //$("#articleRows > div").remove();
           $("#articleRows > button").remove();

           var total = $("#articleRows").attr("totalcount");
            var size = parseInt(total) + Math.min(BATCH_SIZE, articles.length);
            $("#articleRows").attr("totalcount", "" + size);


            var parentRow = null;
            for (var i = 0; i < Math.min(BATCH_SIZE, articles.length); ++i) {


                parentRow = findLastArticleRow();
                insertArticleElement(parentRow, articles[i]);

            }

            if (hasMore) {
                var spanBtn = $("<span></span>");
                spanBtn.attr("class", "col-sm-11");

                var moreBtn = $("<button class=\"btn \">Load More</button>");
                moreBtn.attr("id",  "moreBtn");
                moreBtn.attr("class", "col-sm-1");

                moreBtn.on("click", function () {
                    var offset = $("#articleRows").attr("totalcount");
                    loadFunction(BATCH_SIZE+1, parseInt(offset));
                });


                $("#articleRows").append(spanBtn, moreBtn);
                //$("#moreBtn").show();
            }
        }

        function loadLatestArticles(limit, offset) {

            getLastestArticles(limit, offset, currentUnixTime(),

                function (data) {
                    var articles = data["result"];
                    displayArticles(articles, articles.length > BATCH_SIZE);
                },

                function (data) {
                    alertify.alert("Error\n" + data.responseText, function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                }
            );
        }


        $(document).ready(function () {
            //alertify.confirm('Ocelot Team - Web Blog', 'Welcome!', function(){ alertify.success('Ok') }
                //, function(){ alertify.error('Cancel')});

            /** Search Bar Control  **/
            $('#resultRows').hide();
            $('#searchUsernameInput').hide();
            $('#searchDateFromInput').hide();
            $('#searchDateToInput').hide();

            loadFunction(BATCH_SIZE+1, 0);



        });

        <!-- /.background-->
        function fullheight(){
            var vh =$(window).height();
            $(".header-photo").css({"height": vh + "px"});
        }

        function parallax(){
            var docheight = $(document).height();
            var scrolled;
            $(window).scroll(function(){
                scrolled = $(window).scrollTop();
                $(".header-photo").css({
                    "background-position" : "50% " + ((scrolled/docheight)*100) + "%"
                });
            });
        }

        $(document).ready(function(){
            fullheight();
            parallax();
        });
        $(window).resize(function(){
            fullheight();
            parallax();
        });


    </script>

    <style>
        <c:forEach items="${icons}" var="item" >
        select#avtar option[value="${item}"] {
            background-image: url("${item}");
        }

        </c:forEach>


        iframe {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        @media screen and (max-height: 575px){
            #rc-imageselect,
            .g-recaptcha {transform:scale(0.77);
                -webkit-transform:scale(0.77);
                transform-origin:0 0;
                -webkit-transform-origin:0 0;}
        }


    </style>

</head>
<body>


<header>
    <img src="Photos/good.jpg" width="100%">
    <!--<div class="top">
        <h1>Team Ocelot</h1>
    </div> -->
</header>


<div id="loader-wrapper">
    <div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>


<div id="articleRows" totalCount="0">
    <br>
    <h4><small>RECENT POSTS</small></h4>
    <hr>
</div>

<div id="resultRows">
    <br><br><br>
    <h4><small>SEARCH RESULT</small></h4>
    <hr>
    <div id="tableDiv" class="table-responsive"></div>
</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Login/Registration - Team Ocelot</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8"
                         style="border-right: 1px dotted #C2C2C2;padding-right: 30px; border-left: 1px dotted #C2C2C2;padding-right: 30px;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#Login" data-toggle="tab">Login</a></li>
                            <li><a href="#Registration" data-toggle="tab">Registration</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- Login Tab -->
                            <div class="tab-pane active" id="Login">
                                <form id="loginForm" role="form" action="Login" method="post"
                                      class="form-horizontal">
                                    <div class="form-group">
                                        <label for="username" class="col-sm-2 control-label">
                                            Username</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="username" class="form-control" id="username"
                                                   placeholder="Username"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="passwd" class="col-sm-2 control-label">
                                            Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="passwd" class="form-control" id="passwd"
                                                   placeholder="Password"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                Submit
                                            </button>
                                            <button id="loginCancel" onclick="logout()" type="button" class="btn btn-primary btn-sm">
                                                Cancel
                                            </button>
                                            <p id="error"></p>
                                            <!--<a href="javascript:;">Forgot your password?</a> -->
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- Registration Tab -->
                            <div class="tab-pane" id="Registration">
                                <form id="RegForm" role="form" action="Registration" method="post"
                                      class="form-horizontal">
                                    <div class="form-group">
                                        <label for="email" class="col-sm-3 control-label">
                                            First Name</label>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <select class="form-control" name="gender">
                                                        <option value="male">Mr.</option>
                                                        <option value="female">Ms./Mrs.</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="fname"
                                                           placeholder="First Name"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-3 control-label">
                                            Last Name</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="lname"
                                                   placeholder="Last Name"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="email" class="col-sm-3 control-label">
                                            Username</label>
                                        <div class="col-md-9">
                                            <input id="uname" type="text" class="form-control" name="username"
                                                   placeholder="Username"/>
                                            <span id="unameStatus"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="email" class="col-sm-3 control-label">
                                            Email</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" name="email" id="email"
                                                   placeholder="Email"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="passwdRes" class="col-sm-3 control-label">
                                            Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" name="passwd" id="passwdRes"
                                                   placeholder="Password"/>
                                        </div>
                                    </div>

                                    <!-- Datepicker-->
                                    <div class="form-group">
                                        <label for="dob" class="col-sm-3 control-label">Date of Birth</label>
                                        <div class="col-sm-9 date">
                                            <div class="input-group input-append date" id="datePicker">
                                                <input type="text" class="form-control" id="dob" name="dob"/>
                                                <span class="input-group-addon add-on"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <label class="col-sm-12 control-label">Pick Your Avatar</label>
                                    <iframe  class="embed-responsive-item" id="icon_picker" src="PickAvatar.jsp" height="34%" width="100%">
                                    </iframe>

                                    <div class="form-group">
                                        <label for="countryList" class="col-sm-3 control-label">
                                            Country</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="countryList" name="country">
                                                <option value="">Select your country</option>
                                                <option value="Afganistan">Afghanistan</option>
                                                <option value="Albania">Albania</option>
                                                <option value="Algeria">Algeria</option>
                                                <option value="American Samoa">American Samoa</option>
                                                <option value="Andorra">Andorra</option>
                                                <option value="Angola">Angola</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                                <option value="Argentina">Argentina</option>
                                                <option value="Armenia">Armenia</option>
                                                <option value="Aruba">Aruba</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Austria">Austria</option>
                                                <option value="Azerbaijan">Azerbaijan</option>
                                                <option value="Bahamas">Bahamas</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Bolivia">Bolivia</option>
                                                <option value="Bonaire">Bonaire</option>
                                                <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina
                                                </option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Brazil">Brazil</option>
                                                <option value="British Indian Ocean Ter">British Indian Ocean Ter
                                                </option>
                                                <option value="Brunei">Brunei</option>
                                                <option value="Bulgaria">Bulgaria</option>
                                                <option value="Burkina Faso">Burkina Faso</option>
                                                <option value="Burundi">Burundi</option>
                                                <option value="Cambodia">Cambodia</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Canada">Canada</option>
                                                <option value="Canary Islands">Canary Islands</option>
                                                <option value="Cape Verde">Cape Verde</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Central African Republic">Central African Republic
                                                </option>
                                                <option value="Chad">Chad</option>
                                                <option value="Channel Islands">Channel Islands</option>
                                                <option value="Chile">Chile</option>
                                                <option value="China">China</option>
                                                <option value="Christmas Island">Christmas Island</option>
                                                <option value="Cocos Island">Cocos Island</option>
                                                <option value="Colombia">Colombia</option>
                                                <option value="Comoros">Comoros</option>
                                                <option value="Congo">Congo</option>
                                                <option value="Cook Islands">Cook Islands</option>
                                                <option value="Costa Rica">Costa Rica</option>
                                                <option value="Cote DIvoire">Cote D'Ivoire</option>
                                                <option value="Croatia">Croatia</option>
                                                <option value="Cuba">Cuba</option>
                                                <option value="Curaco">Curacao</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Czech Republic">Czech Republic</option>
                                                <option value="Denmark">Denmark</option>
                                                <option value="Djibouti">Djibouti</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="Dominican Republic">Dominican Republic</option>
                                                <option value="East Timor">East Timor</option>
                                                <option value="Ecuador">Ecuador</option>
                                                <option value="Egypt">Egypt</option>
                                                <option value="El Salvador">El Salvador</option>
                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option value="Eritrea">Eritrea</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Ethiopia">Ethiopia</option>
                                                <option value="Falkland Islands">Falkland Islands</option>
                                                <option value="Faroe Islands">Faroe Islands</option>
                                                <option value="Fiji">Fiji</option>
                                                <option value="Finland">Finland</option>
                                                <option value="France">France</option>
                                                <option value="French Guiana">French Guiana</option>
                                                <option value="French Polynesia">French Polynesia</option>
                                                <option value="French Southern Ter">French Southern Ter</option>
                                                <option value="Gabon">Gabon</option>
                                                <option value="Gambia">Gambia</option>
                                                <option value="Georgia">Georgia</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Gibraltar">Gibraltar</option>
                                                <option value="Great Britain">Great Britain</option>
                                                <option value="Greece">Greece</option>
                                                <option value="Greenland">Greenland</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guadeloupe">Guadeloupe</option>
                                                <option value="Guam">Guam</option>
                                                <option value="Guatemala">Guatemala</option>
                                                <option value="Guinea">Guinea</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Hawaii">Hawaii</option>
                                                <option value="Honduras">Honduras</option>
                                                <option value="Hong Kong">Hong Kong</option>
                                                <option value="Hungary">Hungary</option>
                                                <option value="Iceland">Iceland</option>
                                                <option value="India">India</option>
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Iran">Iran</option>
                                                <option value="Iraq">Iraq</option>
                                                <option value="Ireland">Ireland</option>
                                                <option value="Isle of Man">Isle of Man</option>
                                                <option value="Israel">Israel</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Jamaica">Jamaica</option>
                                                <option value="Japan">Japan</option>
                                                <option value="Jordan">Jordan</option>
                                                <option value="Kazakhstan">Kazakhstan</option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Kiribati">Kiribati</option>
                                                <option value="Korea North">Korea North</option>
                                                <option value="Korea Sout">Korea South</option>
                                                <option value="Kuwait">Kuwait</option>
                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option value="Laos">Laos</option>
                                                <option value="Latvia">Latvia</option>
                                                <option value="Lebanon">Lebanon</option>
                                                <option value="Lesotho">Lesotho</option>
                                                <option value="Liberia">Liberia</option>
                                                <option value="Libya">Libya</option>
                                                <option value="Liechtenstein">Liechtenstein</option>
                                                <option value="Lithuania">Lithuania</option>
                                                <option value="Luxembourg">Luxembourg</option>
                                                <option value="Macau">Macau</option>
                                                <option value="Macedonia">Macedonia</option>
                                                <option value="Madagascar">Madagascar</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Malawi">Malawi</option>
                                                <option value="Maldives">Maldives</option>
                                                <option value="Mali">Mali</option>
                                                <option value="Malta">Malta</option>
                                                <option value="Marshall Islands">Marshall Islands</option>
                                                <option value="Martinique">Martinique</option>
                                                <option value="Mauritania">Mauritania</option>
                                                <option value="Mauritius">Mauritius</option>
                                                <option value="Mayotte">Mayotte</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Midway Islands">Midway Islands</option>
                                                <option value="Moldova">Moldova</option>
                                                <option value="Monaco">Monaco</option>
                                                <option value="Mongolia">Mongolia</option>
                                                <option value="Montserrat">Montserrat</option>
                                                <option value="Morocco">Morocco</option>
                                                <option value="Mozambique">Mozambique</option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="Nambia">Nambia</option>
                                                <option value="Nauru">Nauru</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Netherland Antilles">Netherland Antilles</option>
                                                <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                                <option value="Nevis">Nevis</option>
                                                <option value="New Caledonia">New Caledonia</option>
                                                <option value="New Zealand">New Zealand</option>
                                                <option value="Nicaragua">Nicaragua</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="Niue">Niue</option>
                                                <option value="Norfolk Island">Norfolk Island</option>
                                                <option value="Norway">Norway</option>
                                                <option value="Oman">Oman</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Palau Island">Palau Island</option>
                                                <option value="Palestine">Palestine</option>
                                                <option value="Panama">Panama</option>
                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                <option value="Paraguay">Paraguay</option>
                                                <option value="Peru">Peru</option>
                                                <option value="Phillipines">Philippines</option>
                                                <option value="Pitcairn Island">Pitcairn Island</option>
                                                <option value="Poland">Poland</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="Puerto Rico">Puerto Rico</option>
                                                <option value="Qatar">Qatar</option>
                                                <option value="Republic of Montenegro">Republic of Montenegro</option>
                                                <option value="Republic of Serbia">Republic of Serbia</option>
                                                <option value="Reunion">Reunion</option>
                                                <option value="Romania">Romania</option>
                                                <option value="Russia">Russia</option>
                                                <option value="Rwanda">Rwanda</option>
                                                <option value="St Barthelemy">St Barthelemy</option>
                                                <option value="St Eustatius">St Eustatius</option>
                                                <option value="St Helena">St Helena</option>
                                                <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                                <option value="St Lucia">St Lucia</option>
                                                <option value="St Maarten">St Maarten</option>
                                                <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon
                                                </option>
                                                <option value="St Vincent &amp; Grenadines">St Vincent &amp;
                                                    Grenadines
                                                </option>
                                                <option value="Saipan">Saipan</option>
                                                <option value="Samoa">Samoa</option>
                                                <option value="Samoa American">Samoa American</option>
                                                <option value="San Marino">San Marino</option>
                                                <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                <option value="Senegal">Senegal</option>
                                                <option value="Serbia">Serbia</option>
                                                <option value="Seychelles">Seychelles</option>
                                                <option value="Sierra Leone">Sierra Leone</option>
                                                <option value="Singapore">Singapore</option>
                                                <option value="Slovakia">Slovakia</option>
                                                <option value="Slovenia">Slovenia</option>
                                                <option value="Solomon Islands">Solomon Islands</option>
                                                <option value="Somalia">Somalia</option>
                                                <option value="South Africa">South Africa</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="Sudan">Sudan</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Swaziland">Swaziland</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="Switzerland">Switzerland</option>
                                                <option value="Syria">Syria</option>
                                                <option value="Tahiti">Tahiti</option>
                                                <option value="Taiwan">Taiwan</option>
                                                <option value="Tajikistan">Tajikistan</option>
                                                <option value="Tanzania">Tanzania</option>
                                                <option value="Thailand">Thailand</option>
                                                <option value="Togo">Togo</option>
                                                <option value="Tokelau">Tokelau</option>
                                                <option value="Tonga">Tonga</option>
                                                <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                                                <option value="Tunisia">Tunisia</option>
                                                <option value="Turkey">Turkey</option>
                                                <option value="Turkmenistan">Turkmenistan</option>
                                                <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                                                <option value="Tuvalu">Tuvalu</option>
                                                <option value="Uganda">Uganda</option>
                                                <option value="Ukraine">Ukraine</option>
                                                <option value="United Arab Erimates">United Arab Emirates</option>
                                                <option value="United Kingdom">United Kingdom</option>
                                                <option value="United States of America">United States of America
                                                </option>
                                                <option value="Uraguay">Uruguay</option>
                                                <option value="Uzbekistan">Uzbekistan</option>
                                                <option value="Vanuatu">Vanuatu</option>
                                                <option value="Vatican City State">Vatican City State</option>
                                                <option value="Venezuela">Venezuela</option>
                                                <option value="Vietnam">Vietnam</option>
                                                <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                                <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                                <option value="Wake Island">Wake Island</option>
                                                <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                                                <option value="Yemen">Yemen</option>
                                                <option value="Zaire">Zaire</option>
                                                <option value="Zambia">Zambia</option>
                                                <option value="Zimbabwe">Zimbabwe</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="desc" class="col-sm-3 control-label">Introduction</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" id="desc" name="description" rows="4"
                                                      cols="15" placeholder="Describe yourself here"></textarea>
                                        </div>
                                    </div>

                                    <div id="imageDiv" class="form-group">
                                        <label for="icon" class="col-sm-3 control-label"></label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="icon" name="icon"/>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="g-recaptcha"
                                                 data-sitekey="6LdECUIUAAAAAOxgbtpbsU6qOMi9l2-ha7206cjq"></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-9">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                Save & Continue
                                            </button>
                                            <button type="reset" class="btn btn-default btn-sm">
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<c:if test="${fail}">
    <script>
        $(function () {
            $('#myModal').modal('show');
            console.log("fail reason : " + "${failReason}");
            $('#error')[0].innerHTML = "${failReason}";
        })
    </script>
</c:if>

<script>

    /**============================
     * Search Bar Options Control **/

    function showTitleSearchBar() {
        $('#searchTitleInput').show();

        $('#searchUsernameInput').hide();
        $('#searchUsernameInput').val("");

        $('#searchDateFromInput').hide();
        $('#searchDateFromInput').val("");

        $('#searchDateToInput').hide();
        $('#searchDateToInput').val("");

        $('#datePickerSearchFrom').hide();
        $('#datePickerSearchTo').hide();
    }

    function showUsernameSearchBar() {
        $('#searchUsernameInput').show();

        $('#searchTitleInput').hide();
        $('#searchTitleInput').val("");

        $('#searchDateFromInput').hide();
        $('#searchDateFromInput').val("");

        $('#searchDateToInput').hide();
        $('#searchDateToInput').val("");

        $('#datePickerSearchFrom').hide();
        $('#datePickerSearchTo').hide();

    }

    function showDateSearchBar() {
        $('#searchDateFromInput').show();
        $('#searchDateToInput').show();

        $('#datePickerSearchFrom').show();
        $('#datePickerSearchTo').show();

        $('#searchTitleInput').hide();
        $('#searchTitleInput').val("");

        $('#searchUsernameInput').hide();
        $('#searchUsernameInput').val("");
    }
    /** Search Bar Options Control
     * ============================**/


    function setUserNameStatus(available) {
        if (available) {
            $("#unameStatus").text("This name is available");
            $("#unameStatus").css("color", "green");
        } else {
            $("#unameStatus").text("This name is unavailable");
            $("#unameStatus").css("color", "red");
        }
    }


    function search() {

        $('#resultTable').show();

        if ($('#searchTitleInput').val() != ""){

            searchByTitle($('#searchTitleInput').val(), true);
        }

        if ($('#searchDateFromInput').val()!="" && $('#searchDateToInput').val()!=""){

            var dateFrom = new Date($('#searchDateFromInput').val());
            var dateTo = new Date($('#searchDateToInput').val());

            searchByDate(dateFrom.getTime(), dateTo.getTime(), true);
        }

        if ($('#searchUsernameInput').val() != ""){
            searchByUsername($('#searchUsernameInput').val(), true);
        }

    }
    
    
    var searchedArticles = null;



    function sortByDate(asc) {
        searchedArticles.sort(function (d1, d2) {
            if (asc) {
                return d1.pubtime - d2.pubtime;
            } else {
                return d2.pubtime - d1.pubtime;
            }
        })
    }
    
    
    function sortByName(asc) {

        searchedArticles.sort(
            function (d1, d2) {
                if (asc) {
                    return d1.userName < d2.userName;
                } else {
                    return d2.userName < d1.userName;
                }
            }
        );
    }

    function sortByTitle(asc) {

        searchedArticles.sort(
            function (d1, d2) {
                if (asc) {
                    return d1.title < d2.title;
                } else {
                    return d2.title < d1.title;
                }
            }
        );
    }
    


    function searchByUsername(username, asc){

        var rule = {};
        rule.field = "userName";
        rule.asc = asc;
        rule.value1 = username;
        rule.value2 = null;

        var rules = [];
        rules.push(rule);

        searchArticles(50, 0, rules, currentUnixTime(),
            function (data) {
            
                searchedArticles = data.result;

                $('#articleRows').hide();

                setAsc("name", true);

                sortByTitle(true);

                generateResult();

                $('#resultRows').show();

            },

            function (data) {
                alertify.alert("Error: searchArticles ByUsername " + JSON.stringify(data), function () {
                    alertify.message('OK');
                }).setHeader('<em> Team Ocelot </em> ');
            }

        )

    }


    function searchByDate (dateFrom, dateTo, asc){

        var rule ={};
        rule.field = "pubtime";
        rule.asc = asc;
        rule.value1 = dateFrom;
        rule.value2 = dateTo;

        var rules = [];
        rules.push(rule);

        searchArticles(50, 0, rules, currentUnixTime(),
            function (data) {


                searchedArticles = data.result;

                $('#articleRows').hide();

                setAsc("date", true);

                sortByDate(true);

                generateResult();

                $('#resultRows').show();

            },

            function (data) {
                alertify.alert("Error: searchArticles ByDate " + JSON.stringify(data), function () {
                    alertify.message('OK');
                }).setHeader('<em> Team Ocelot </em> ');

            }
        )
    }

    function searchByTitle(title, asc){

        var rule = {};
        rule.field = "title";
        rule.asc = asc;
        rule.value1 = title;
        rule.value2 = null;

        var rules = [];
        rules.push(rule);

        searchArticles(50, 0, rules, currentUnixTime(),
            function (data) {

                searchedArticles = data.result;

                $('#articleRows').hide();

                setAsc("date", true);

                sortByTitle(true);

                generateResult();

                $('#resultRows').show();

            },

            function (data) {
                alertify.alert("Error: searchArticles ByTitle " + JSON.stringify(data), function () {
                    alertify.message('OK');
                }).setHeader('<em> Team Ocelot </em> ');
            }

        )

    }

    function setAsc(field, asc) {

        $("#titleSpan").prop("sort", false);
        $("#nameSpan").prop("sort", false);
        $("#dateSpan").prop("sort", false);

        var span = null;
        if (field == "title") {
            span = $("#titleSpan");
            $("#titleSpan").prop("sort", true);
        } else if (field == "name"){
            span = $("#nameSpan");
            $("#nameSpan").prop("sort", true);
        } else if (field == "date") {
            span = $("#dateSpan");
            $("#dateSpan").prop("sort", true);
        }

        if (asc) {
            span.attr("class", "glyphicon glyphicon-sort-by-attributes");
        } else {
            span.attr("class", "glyphicon glyphicon-sort-by-attributes-alt");
        }
        span.prop("asc", asc);

    }

    function getAsc(field) {
        var span = null;
        if (field == "title") {
            span = $("#titleSpan");
        } else if (field == "name"){
            span = $("#nameSpan");
        } else if (field == "date") {
            span = $("#dateSpan");
        }

        return span.prop("asc");
    }



    function generateEmptyTable() {

        var tableTag = $("<table></table>");
        tableTag.attr("id", "resultTable");
        tableTag.attr("class", "table table-striped table-bordered");

        var tableThead = $("<thead></thead>");

        var tableTr = $("<tr></tr>");
        tableTag.append(tableTr);

        var tableTh1 = $("<th></th>");
        tableTh1.append("Article ID");



        var tableTh2 = $("<th></th>");
        tableTh2.append("Article Title");

        var tableTh2Span = $("<span></span>");
        tableTh2Span.attr("class", "glyphicon glyphicon-sort-by-attributes");
        tableTh2Span.attr("id", "titleSpan");

        tableTh2.append(tableTh2Span);

        var tableTh2Link = $("<a></a>");
        tableTh2Link.append(tableTh2Span);
        tableTh2Link.attr("href", "#");
        tableTh2Link.on("click", function(){

            var asc = getAsc("title");
            setAsc("title", !asc);
            sortByTitle(!asc);
            generateResult();

        });
        tableTh2.append(tableTh2Link);


        var tableTh3 = $("<th></th>");
        tableTh3.append("Username");

        var tableTh3Span = $("<span></span>");
        tableTh3Span.attr("id", "nameSpan");

        tableTh3Span.attr("class", "glyphicon glyphicon-sort-by-attributes");

        tableTh3.append(tableTh3Span);

        var tableTh3Link = $("<a></a>");
        tableTh3Link.append(tableTh3Span);
        tableTh3Link.attr("href", "#");

        tableTh3Link.on("click", function(){

            var asc = getAsc("name");
            setAsc("name", !asc);
            sortByName(!asc);
            generateResult();

        });

        tableTh3.append(tableTh3Link);


        var tableTh4 = $("<th></th>");
        tableTh4.append("Article Date & Time");

        var tableTh4Span = $("<span></span>");
        tableTh4Span.attr("id", "dateSpan");

        tableTh4Span.attr("class", "glyphicon glyphicon-sort-by-attributes");
        tableTh4.append(tableTh4Span);

        var tableTh4Link = $("<a></a>");
        tableTh4Link.append(tableTh4Span);
        tableTh4Link.attr("href", "#");

        tableTh4Link.on("click", function(){

            var asc = getAsc("date");
            setAsc("date", !asc);
            sortByDate(!asc);
            generateResult();

        });

        tableTh4.append(tableTh4Link);


        tableTr.append(tableTh1, tableTh2, tableTh3, tableTh4);
        tableThead.append(tableTr);
        tableTag.append(tableThead);

        $('#tableDiv').append(tableTag);

    }

    function generateData() {

        var searchResult = searchedArticles;

        $("#resultTable > tbody").remove();

        var tableBody  = $("<tbody></tbody>");
        $('#resultTable').append(tableBody);

        for (var i = 0; i < searchResult.length; ++i) {

            console.log("" + searchResult[i].title);

            var searchTableTr = $("<tr></tr>");
            searchTableTr.attr("searchTableTrId", searchResult[i].id);

            var articleIdTd = $("<td></td>");
            articleIdTd.attr("articleIdId", searchResult[i].id);
            articleIdTd.append(searchResult[i].id);

            var articleTitleTd = $("<td></td>");
            articleTitleTd.attr("articleTitleTdId", searchResult[i].id);

            var articleTitle = $("<span></span>");
            articleTitle.append(searchResult[i].title)

            var articleTitleLink = $("<a></a>");
            articleTitleLink.attr("href", "ViewSingleArticleServlet?article_id=" + searchResult[i].id);
            articleTitleLink.append(articleTitle);
            articleTitleTd.append(articleTitleLink);

            var articleUsernameTd = $("<td></td>");
            articleUsernameTd.attr("articleUsernameTdId", searchResult[i].id);
            articleUsernameTd.append(searchResult[i].userName);

            var articlePubtimeTd = $("<td></td>");
            articlePubtimeTd.attr("articlePubtimeTdId", searchResult[i].id);
            var date = new Date(searchResult[i].pubtime);
            articlePubtimeTd.append(date);

            searchTableTr.append(articleIdTd, articleTitleTd, articleUsernameTd, articlePubtimeTd);
            tableBody.append(searchTableTr)

        }
    }

    function generateResult(){
        generateData();
    }

    function checkUserName() {

        var name = $("#uname").val();
        if (name.length <= 0) {
            $("#unameStatus").text("UserName can not be empty!");
            $("#unameStatus").css("color", "red");
            return;
        }


        getUserByUserName(name,
            function (data) {
                var user = data.result;
                if (user == null) {
                    setUserNameStatus(true);
                } else {
                    setUserNameStatus(false);
                }
            },

            function (data) {
                alertify.alert("getUserByUserName " + JSON.stringify(data), function () {
                    alertify.message('OK');
                }).setHeader('<em> Team Ocelot </em> ');
            }
        );
    }

    function logout(){
        window.location.reload();
    }




    $(document).ready(function () {

        generateEmptyTable();
        $('#resultTable').hide();


        $("#uname").on("focusout", checkUserName);
        $("#uname").on("focusin", function () {
            $("#unameStatus").text("");
        });

        $('#datePicker')
            .datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'

            }).datepicker("setDate", '1990-01-01');

        $('#datePickerSearchFrom')
            .datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'

            }).datepicker("setDate", '2018-02-01');

        $('#datePickerSearchTo')
            .datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'

            }).datepicker("setDate", '2018-02-05');

        $('#imageDiv').hide();



        /** Generate LoginLogout Elements **/
        var loginButtonElement = $("<button></button>");
        loginButtonElement.attr("id", "loginButton");
        loginButtonElement.attr("class", "btn btn-primary");
        loginButtonElement.attr("data-toggle", "modal");
        loginButtonElement.attr("data-target", "#myModal");
        loginButtonElement.append("Login");

        var formLogoutElement = $("<form></form>");
        formLogoutElement.attr("action", "Logout");

        var logoutButtonElement = $("<button></button>");
        logoutButtonElement.attr("type", "submit");
        logoutButtonElement.attr("id", "logoutButton");
        logoutButtonElement.attr("class", "btn btn-primary");
        logoutButtonElement.append("Logout");
        formLogoutElement.append(logoutButtonElement);
        $('#loginLogout').append(loginButtonElement, formLogoutElement);


        /** When User Login show logout button, hide the login button.
         * Also show "myPage" link on navBar
         * **/
        if ("${userSession.userName}" != ""){

            $('#loginButton').hide();
            $('#logoutButton').show();
            $('#myPage').show();
        } else {
            $('#loginButton').show();
            $('#logoutButton').hide();
            $('#myPage').hide();
        }


        /** Only display when admin login**/

        if ("${userSession.userName}" == "admin")
        {
            var adminList = $("<li></li>");

            var adminLink = $("<a></a>");
            adminLink.attr("href", "Admin.jsp");

            var adminSpan = $("<span></span>");
            adminSpan.attr("class", "glyphicon glyphicon-list");

            adminList.append(adminLink);
            adminLink.append(adminSpan);
            adminLink.append("Admin Page");

            adminList.insertBefore($('#dropdown'));

        }

    });

    function setIcon(icon) {
        $("#icon").attr("value", icon);
        console.log("setIcon " + icon);
    }

    // Resize reCAPTCHA to fit width of container
    // captchaScale = containerWidth / elementWidth



</script>

<script type="text/javascript" src="js/EditArticle.js"></script>

</body>
</html>