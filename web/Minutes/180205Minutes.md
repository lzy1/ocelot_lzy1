Agenda
=======
1. To complete search function for main page
2. To perfect the design and layout of the website

Minutes/Notes
=======
1.Completed search function feature for main page
- Every group member involved in the coding and implementation of search function feature for main page.

2.Standardised navigation bar layout for all pages
- Every group member involved in the standardisation of navigation bar layout for all pages.

Next Steps:
=======
1. To make navigation bar for all websites responsive.
2. To make all websites layout responsive.
3. To upload our work to server. 
