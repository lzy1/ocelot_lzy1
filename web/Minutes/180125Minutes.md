Agenda
=======
1. To discuss about retrieving data from database and include the data in our main page and personal page
2. To discuss about linking function to button and navigation bars of main page and personal page.
3. To discuss about how to implement nested comment.

Minutes/Notes
=======
1.Successfully link database to registration page 
- Every group member involved in the coding and linking of storing data from registration page to database.

2.To discuss about design and implementation of session
- Every group member involved in the discussion about designing and implementation of session.

3.Successfully link database to login page
- Every group member involved in writing code of linking login page details to connect with database.

Next Steps:
=======
1. To design and implement admin function 
2. To discuss about how to retrieve article from database.
3. To discuss about how to make browser remember that user had logged in.
