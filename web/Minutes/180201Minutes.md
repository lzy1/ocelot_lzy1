Agenda
=======
1. To implement admin function
2. To successfully implement upload picture function

Minutes/Notes
=======
1.Successfully retrieve data from database to admin page
- Every group member involved in the coding of retrieving data from database to admin page

2.Successfully implement upload picture function
- Every group member involved in the coding of upload picture function

3.Redesign successfully registered account page
- Every group member involved in the coding and design of successfully registered page

4. Redesign edit details page
- Every group member involved in the coding and design of edit details page

Next Steps:
=======
1. To implement remove user function on admin page.
2. To implement search article function on main page and user page.
