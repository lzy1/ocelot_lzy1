<%--
  Created by IntelliJ IDEA.
  User: yt172
  Date: 29/01/2018
  Time: 3:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <title>Account Information</title>
    <script src="js/Helper.js"></script>
    <script src="js/ajax/Article.js"></script>
    <script src="js/ajax/Comment.js"></script>
    <script src="js/ajax/User.js"></script>

    <!-- Include Bootstrap Datepicker -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>


    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/bootstrap.min.css"/>


    <style>

        .sample-header {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 30%;
            background-image: url("Photos/meow.jpg");
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }

        .sample-header::before {
            content: "";
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: MidnightBlue;
            opacity: 0.3;
        }

        .sample-header-section {
            position: relative;
            padding: 15% 0 10%;
            max-width: 300px;
            margin-left: auto;
            margin-right: auto;
            color: white;
            text-shadow: 1px 1px 4px rgba(0, 0, 0, 0.5);
            font-family: "Montserrat", sans-serif;
        }

        h1 {
            font-weight: 300;
            text-decoration: underline;
            margin-right: auto;
            margin-left: auto;
        }

        h2 {
            font-weight: 100;
        }

        p {
            font-family: "Dancing Script", cursive;
        }

        .sample-section {
            position: relative;
            max-width: 640px;
            margin-left: auto;
            margin-right: auto;
            padding: 40px;
            background-color: white;
        }


    </style>


</head>

<body>
<div class="container">

    <div class="sample-header">
        <div class="sample-header-section">
            <h2>Team ocelot</h2>
        </div>
    </div>

    <div class="sample-section">

        <div class="row">
            <div class="col-sm-2"></div>

            <div class="col-sm-8">
                <h2>General account settings</h2>
            </div>

            <div class="col-sm-2"></div>
        </div>

        <form id="editForm" role="form" action="UserServlet" method="post" class="form-horizontal">
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-md-3">
                            <select id="gender" class="form-control" name="gender" value="${userSession.gender}">
                                <option value="male">Mr.</option>
                                <option value="female">Ms./Mrs.</option>
                            </select>
                        </div>
                        <div class="col-md-9">
                            <input id="fname" type="text" class="form-control" name="fname"
                                   value="${userSession.fname}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Last Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="lname" value="${userSession.lname}"/>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Username</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="username" value="${userSession.userName}" disabled/>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" name="email" id="email"
                           value="${userSession.email}"/>
                </div>
            </div>

            <div class="form-group">
                <label for="passwd" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="passwd" id="passwd"
                           value="${userSession.passwd}"/>
                </div>
            </div>

            <!-- Datepicker-->
            <div class="form-group">
                <label for="dob" class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9 date">
                    <div class="input-group input-append date" id="datePicker">
                        <input type="text" class="form-control" id="dob" name="dob" value="${userSession.dob}"/>
                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
            <!-- Datepicker-->

            <label class="col-sm-12 control-label">Pick Your Avatar</label>
            <iframe class="embed-responsive-item" id="icon_picker" src="PickAvatar.jsp" height="34%" width="100%">
            </iframe>

            <div class="form-group">
                <label for="country" class="col-sm-3 control-label">
                    Country</label>
                <div class="col-sm-9">
                    <select class="form-control" id="country" name="country">
                        <option value="">Select your country</option>
                        <option value="Afganistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bonaire">Bonaire</option>
                        <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Canary Islands">Canary Islands</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Channel Islands">Channel Islands</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos Island">Cocos Island</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote DIvoire">Cote D'Ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Curaco">Curacao</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Ter">French Southern Ter</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Great Britain">Great Britain</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Hawaii">Hawaii</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran">Iran</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Isle of Man">Isle of Man</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Korea North">Korea North</option>
                        <option value="Korea Sout">Korea South</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Laos">Laos</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libya">Libya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macau">Macau</option>
                        <option value="Macedonia">Macedonia</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Midway Islands">Midway Islands</option>
                        <option value="Moldova">Moldova</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Nambia">Nambia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherland Antilles">Netherland Antilles</option>
                        <option value="Netherlands">Netherlands (Holland, Europe)</option>
                        <option value="Nevis">Nevis</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau Island">Palau Island</option>
                        <option value="Palestine">Palestine</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Phillipines">Philippines</option>
                        <option value="Pitcairn Island">Pitcairn Island</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Republic of Montenegro">Republic of Montenegro</option>
                        <option value="Republic of Serbia">Republic of Serbia</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russia</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="St Barthelemy">St Barthelemy</option>
                        <option value="St Eustatius">St Eustatius</option>
                        <option value="St Helena">St Helena</option>
                        <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                        <option value="St Lucia">St Lucia</option>
                        <option value="St Maarten">St Maarten</option>
                        <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                        <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                        <option value="Saipan">Saipan</option>
                        <option value="Samoa">Samoa</option>
                        <option value="Samoa American">Samoa American</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Serbia">Serbia</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syria">Syria</option>
                        <option value="Tahiti">Tahiti</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Erimates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States of America">United States of America</option>
                        <option value="Uraguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Vatican City State">Vatican City State</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                        <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                        <option value="Wake Island">Wake Island</option>
                        <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zaire">Zaire</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                    </select>
                </div>
            </div>

            <!--<div class="form-group">
                <label for="passwdRes" class="col-sm-3 control-label">
                    Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="passwd" id="passwdRes"
                           placeholder="Password"/>
                </div>
            </div>-->

            <div class="form-group">
                <label for="desc" class="col-sm-3 control-label">Introduction</label>
                <div class="col-sm-9">
                    <textarea class="form-control" id="desc" name="description" rows="4"
                              cols="15"> ${userSession.description} </textarea>
                </div>
            </div>

            <div id="imageDiv" class="form-group">
                <label for="icon" class="col-sm-3 control-label"></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="icon" name="icon"/>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary btn-sm">
                        Save & Continue
                    </button>
                    <button type="button" onclick="changeCancel" class="btn btn-default btn-sm"><a href="Profile.jsp">
                        Cancel</a>
                    </button>
                </div>
            </div>

        </form>
    </div>
    <script type='text/javascript'>

        function changeCancel() {
            window.location.replace("Profile.jsp");
        }

        function setIcon(icon) {
            $("#icon").attr("value", icon);
            console.log("setIcon " + icon);
        }


        $(function () {

            $("#sendMail").click(
                function () {
                    open("SendMailForResetPasswordServlet");
                }
            );


            $("#country").val("${userSession.country}");
            $("#gender").val("${userSession.gender}");


            $("#editForm").submit(function (event) {

                /* stop form from submitting normally */
                event.preventDefault();

                /* get the action attribute from the <form action=""> element */
//            var $form = $( this );
//            var url = $form.attr( 'action' );
//
//            /* Send the data using post with element id name and name2*/
//            var posting = $.post( url, { name: $('#name').val(), name2: $('#name2').val() } );
//
//            /* Alerts the results */
//            posting.done(function( data ) {
//                alert('success');
//            });

                var user = {
                    id: "${userSession.id}",
                    fname: $("#fname").val(),
                    lname: $("#lname").val(),
                    passwd: $("#passwd").val(),
                    gender: $("#gender").val(),
                    email: $("#email").val(),
                    icon: $("#icon").val(),
                    dob: $("#dob").val(),
                    country: $("#country").val(),
                    description: $("#desc").val()
                };

                modifyUser(user,
                    function (data) {
                        alertify
                            .alert("you info is saved!", function () {
                                alertify.message('OK');
                            }).setHeader('<em> Team Ocelot </em> ');
                        updateUserSession(
                            function (data) {

                                alertify.confirm('UpdateUserSession succeed!', function(){ window.location.replace("Profile.jsp"); }).setHeader('<em> Team Ocelot </em> ');


                            },

                            function (data) {
                                alertify
                                    .alert("updateUserSession failed!", function () {
                                        alertify.message('OK');
                                    }).setHeader('<em> Team Ocelot </em> ');


                            }
                        );

                    },

                    function (data) {
                        alertify.alert("modifyUser failed " + JSON.stringify(data)).setHeader('<em> Team Ocelot </em> ');

                    }
                )


            });

            $('#datePicker')
                .datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'

                })

            //appends an "active" class to .popup and .popup-content when the "Open" button is clicked
            $(".open").on("click", function () {
                $(".popup-overlay, .popup-content").addClass("active").toggle(".overlay").show(".blur");
            });

//removes the "active" class to .popup and .popup-content when the "Close" button is clicked
            $(".close").on("click", function () {
                $(".popup-overlay, .popup-content").removeClass("active");
            });

            function parallax_height() {
                var scroll_top = $(this).scrollTop();
                var sample_section_top = $(".sample-section").offset().top;
                var header_height = $(".sample-header-section").outerHeight();
                $(".sample-section").css({"margin-top": header_height});
                $(".sample-header").css({height: header_height - scroll_top});
            }

            parallax_height();
            $(window).scroll(function () {
                parallax_height();
            });
            $(window).resize(function () {
                parallax_height();
            });
        });

        /* attach a submit handler to the form */

        function setIcon(icon) {
            $("#imageDiv").hide();

            $("#icon").attr("value", icon);
            console.log("setIcon " + icon);
        }

    </script>
</div>
</body>
</html>
