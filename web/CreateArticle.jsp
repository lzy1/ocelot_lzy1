<%--
  Created by IntelliJ IDEA.
  User: lzy1
  Date: 28/01/2018
  Time: 2:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>


    <script type="text/javascript" src="js/ajax/Article.js"></script>


    <!-- include libraries(jQuery, bootstrap) -->
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>


    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <!-- JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/bootstrap.min.css"/>
    <!-- Include Bootstrap Datepicker -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <!-- Include cdn for Timepicker -->
    <script type="text/javascript" src="js/vendor/clockface.js"></script>
    <link rel="stylesheet" href="css/vendor/clockface.css">



    <style type="text/css">
        #eventForm {
            top: 0;
            right: -5px;
        }




    </style>

    <title>CreateArticle</title>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="Menu.jsp"><span class="glyphicon glyphicon-home"></span>Main Page</a></li>
            <li><a href="Profile.jsp"><span class="glyphicon glyphicon-user"></span>My Page</a></li>
        </ul>
        </div>
        </div>
    </nav>
    <!-- /.navbar-collapse -->

</head>

<body>


<!-- Titlebar-->
<form id="eventForm" method="post" class="form-horizontal">
    <div class="form-group">

        <label class="col-xs-3 control-label">Title</label>
        <div class="col-xs-5">
            <input type="text" id="title" class="form-control" name="title" />
        </div>

    </div>


    <!-- Summernote-->
    <div class="form-group">
        <label class="col-xs-3 control-label">Content</label>
        <div class="col-xs-5">
            <textarea id="summernote" name="content"></textarea>
        </div>
    </div>
    <!-- Summernote-->



    <!-- Datepicker-->
    <div class="form-group">
        <label class="col-xs-3 control-label">Date</label>
        <div class="col-xs-5 date">
            <div class="input-group input-append date" id="datePicker">
                <input type="text" class="form-control" id="date" name="date" />
                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
    <!-- Datepicker-->
    <div class="form-group">
    <label class="col-xs-3 control-label">Select Time:</label>
    <!-- Input group, just add class 'clockpicker', and optional data-* -->
    <div class="col-xs-5 container">
        <input type="text" id="time" name="time" class="sel-time form-control">
    </div>
    </div>


    <!-- Publish and cancel button-->
    <div class="form-group">
        <div class="col-xs-5 col-xs-offset-3">
            <button type="button" class="btn btn-default" onclick="publish()">Publish</button>
            <button type="button" class="btn btn-default" onclick="cancelClick()">Clear</button>
        </div>
    </div>
    <!-- Publish and cancel button-->


</form>

<script>
    $(function() {
        $('#summernote').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            placeholder: 'Please type your content here',
            tabsize: 2,
            height: 200,
            width: 600
        });


        $('#datePicker')
            .datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'

            }).datepicker("setDate",'now')
        // Time Picker Initialization
        $('.sel-time').clockface({format: 'HH:mm'});
        $('.sel-time-am').clockface();


        if($('#summernote').val() ==""){

        };

    });


    function cancelClick () {
        document.execCommand('Stop')
    }

    function publish () {
        var d = null;
        var title = $("#title").val();
        var content = $("#summernote").val();
        var date = $("#date").val();
        var time = $("#time").val();

        if (time ===""){
            d = new Date();
        } else{
            d = new Date(date + " " + time);
        }

        var pubtime = d.getTime();
        var user_id = "${userSession.id}";


        if($('#summernote').val() ==""){
            alertify
                .alert("Please enter your article content");
        } else {

        addArticle(user_id, title, content,
            function (data) {
                alertify.confirm("Add Article success", function(){ window.location.replace("Profile.jsp"); });

                },
            function (data) {
                alertify
                    .alert("Add Article failed"+ JSON.stringify(data));
            },
            pubtime)

        }
    }


</script>
</body>
</html>
