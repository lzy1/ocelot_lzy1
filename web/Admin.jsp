<%--
  Created by IntelliJ IDEA.
  User: lzy1
  Date: 27/01/2018
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="org.jooq.util.derby.sys.Sys" %>
<%@ page import="ocelot.application.beans.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<meta name="viewport" content="width=1024">
<head>
    <title>AdminPage</title>
    <link rel="icon" type="image/jpg" href="Photos/ocelot.jpg" />

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>

    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/bootstrap.min.css"/>

    <!-- Include Bootstrap Datepicker -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>


    <link rel = "stylesheet" type = "text/css" href = "Admin.css" />
    <script type="text/javascript" src="js/Helper.js"></script>
    <script type="text/javascript" src="js/ajax/Article.js"></script>
    <script type="text/javascript" src="js/ajax/User.js"></script>
    <script type="text/javascript" src="js/ajax/Comment.js"></script>
    <link href="Login.css" rel="stylesheet" type="text/css">


    <!-- Collect the nav links, forms, and other content for toggling -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul id="adminPageBar" class="nav navbar-nav">
             <li><a href="Menu.jsp"><span class="glyphicon glyphicon-home"></span>Main Page</a></li>
             <li><a href="Profile.jsp"><span class="glyphicon glyphicon-user"></span>My Page</a></li>
                <li><a href="Admin.jsp"><span class="glyphicon glyphicon-list"></span>Admin Page</a></li>
             <!--<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">MyHistory <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">History</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Comment</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Photos</a></li>
                </ul>
            </li>-->
        </ul>

        <ul class="nav navbar-nav navbar-right">

            <li><a href="CreateArticle.jsp">Create Article<span class="glyphicon glyphicon-file"></span></a></li>

        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">Edit Details <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="SelfDetail.jsp">Change Account Info</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Deactivate Account</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="Logout">Logout</a></li>
                </ul>
            </li>
        </ul>

        <!--<form class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Search</button>

        </form>-->
        </div>
    </div>
    </nav>
    <!-- /.navbar-collapse -->


</head>

<body>
<%
    System.out.println(session.getAttribute("userSession")+ "session");
    //allow access only if session exists
    User user = (User) session.getAttribute("userSession");
    if( user == null || !user.getUserName().equals("admin") ){
        response.sendRedirect("Menu.jsp");
    }

%>

<div class="container">

<div class="page-header"><h1>Welcome Admin</h1></div>

<!-- AddUser -->
<button class="accordion">User</button>
<div class="panel">

<!-- AddUser -->

<!-- RemoveUser -->
<button class="accordion">Add/Remove Users</button>
<div class="panel">
    <!-- Table -->
    <table id="userTable" class="table table-striped">
        <tr>
            <th>UserID</th>
            <th>Username</th>
            <th>Date of Birth</th>
            <th>Country</th>
            <th>Actions</th>
        </tr>

        <!--<tr>
            <!-- To get UserID from database -->
            <td></td>
            <!-- To get PostID from database -->
            <td></td>
            <!-- To get DateCreated from database -->
            <td></td>
            <!-- To show whether post is hide or show -->
            <td></td>
            <td>
                <!--Creates the popup body-->
                <div class="popup-overlay">
                    <!--Creates the popup content-->
                    <div class="popup-content">
                        <form id="editForm" role="form" action="UserServlet" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select id="gender" class="form-control" name="gender" value="${userSession.gender}">
                                                <option value="male">Mr.</option>
                                                <option value="female">Ms./Mrs.</option>
                                            </select>
                                        </div>
                                        <div class="col-md-9">
                                            <input id="fname" type="text" class="form-control" name="fname">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Last Name</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="lname" name="lname">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Username</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="username" name="username">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email" id="email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="passwd" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="passwd" id="passwd">
                                </div>
                            </div>

                            <!-- Datepicker-->
                            <div class="form-group">
                                <label for="dob" class="col-sm-3 control-label">Date of Birth</label>
                                <div class="col-sm-9 date">
                                    <div class="input-group input-append date" id="datePicker">
                                        <input type="text" class="form-control" id="dob" name="dob">
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <!-- Datepicker-->

                            <div class="form-group">
                                <label for="country" class ="col-sm-3 control-label">
                                    Country</label>
                                <div class="col-sm-9">
                                    <select  class="form-control" id="country" name="country" >
                                        <option value="">Select your country</option>
                                        <option value="Afganistan">Afghanistan</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrain">Bahrain</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Belgium">Belgium</option>
                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermuda">Bermuda</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Bonaire">Bonaire</option>
                                        <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                        <option value="Botswana">Botswana</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                        <option value="Brunei">Brunei</option>
                                        <option value="Bulgaria">Bulgaria</option>
                                        <option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option>
                                        <option value="Cambodia">Cambodia</option>
                                        <option value="Cameroon">Cameroon</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Canary Islands">Canary Islands</option>
                                        <option value="Cape Verde">Cape Verde</option>
                                        <option value="Cayman Islands">Cayman Islands</option>
                                        <option value="Central African Republic">Central African Republic</option>
                                        <option value="Chad">Chad</option>
                                        <option value="Channel Islands">Channel Islands</option>
                                        <option value="Chile">Chile</option>
                                        <option value="China">China</option>
                                        <option value="Christmas Island">Christmas Island</option>
                                        <option value="Cocos Island">Cocos Island</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="Comoros">Comoros</option>
                                        <option value="Congo">Congo</option>
                                        <option value="Cook Islands">Cook Islands</option>
                                        <option value="Costa Rica">Costa Rica</option>
                                        <option value="Cote DIvoire">Cote D'Ivoire</option>
                                        <option value="Croatia">Croatia</option>
                                        <option value="Cuba">Cuba</option>
                                        <option value="Curaco">Curacao</option>
                                        <option value="Cyprus">Cyprus</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option>
                                        <option value="Dominican Republic">Dominican Republic</option>
                                        <option value="East Timor">East Timor</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="Egypt">Egypt</option>
                                        <option value="El Salvador">El Salvador</option>
                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                        <option value="Eritrea">Eritrea</option>
                                        <option value="Estonia">Estonia</option>
                                        <option value="Ethiopia">Ethiopia</option>
                                        <option value="Falkland Islands">Falkland Islands</option>
                                        <option value="Faroe Islands">Faroe Islands</option>
                                        <option value="Fiji">Fiji</option>
                                        <option value="Finland">Finland</option>
                                        <option value="France">France</option>
                                        <option value="French Guiana">French Guiana</option>
                                        <option value="French Polynesia">French Polynesia</option>
                                        <option value="French Southern Ter">French Southern Ter</option>
                                        <option value="Gabon">Gabon</option>
                                        <option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option>
                                        <option value="Great Britain">Great Britain</option>
                                        <option value="Greece">Greece</option>
                                        <option value="Greenland">Greenland</option>
                                        <option value="Grenada">Grenada</option>
                                        <option value="Guadeloupe">Guadeloupe</option>
                                        <option value="Guam">Guam</option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guinea">Guinea</option>
                                        <option value="Guyana">Guyana</option>
                                        <option value="Haiti">Haiti</option>
                                        <option value="Hawaii">Hawaii</option>
                                        <option value="Honduras">Honduras</option>
                                        <option value="Hong Kong">Hong Kong</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Iceland">Iceland</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Iran">Iran</option>
                                        <option value="Iraq">Iraq</option>
                                        <option value="Ireland">Ireland</option>
                                        <option value="Isle of Man">Isle of Man</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Jamaica">Jamaica</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Jordan">Jordan</option>
                                        <option value="Kazakhstan">Kazakhstan</option>
                                        <option value="Kenya">Kenya</option>
                                        <option value="Kiribati">Kiribati</option>
                                        <option value="Korea North">Korea North</option>
                                        <option value="Korea Sout">Korea South</option>
                                        <option value="Kuwait">Kuwait</option>
                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                        <option value="Laos">Laos</option>
                                        <option value="Latvia">Latvia</option>
                                        <option value="Lebanon">Lebanon</option>
                                        <option value="Lesotho">Lesotho</option>
                                        <option value="Liberia">Liberia</option>
                                        <option value="Libya">Libya</option>
                                        <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lithuania">Lithuania</option>
                                        <option value="Luxembourg">Luxembourg</option>
                                        <option value="Macau">Macau</option>
                                        <option value="Macedonia">Macedonia</option>
                                        <option value="Madagascar">Madagascar</option>
                                        <option value="Malaysia">Malaysia</option>
                                        <option value="Malawi">Malawi</option>
                                        <option value="Maldives">Maldives</option>
                                        <option value="Mali">Mali</option>
                                        <option value="Malta">Malta</option>
                                        <option value="Marshall Islands">Marshall Islands</option>
                                        <option value="Martinique">Martinique</option>
                                        <option value="Mauritania">Mauritania</option>
                                        <option value="Mauritius">Mauritius</option>
                                        <option value="Mayotte">Mayotte</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Midway Islands">Midway Islands</option>
                                        <option value="Moldova">Moldova</option>
                                        <option value="Monaco">Monaco</option>
                                        <option value="Mongolia">Mongolia</option>
                                        <option value="Montserrat">Montserrat</option>
                                        <option value="Morocco">Morocco</option>
                                        <option value="Mozambique">Mozambique</option>
                                        <option value="Myanmar">Myanmar</option>
                                        <option value="Nambia">Nambia</option>
                                        <option value="Nauru">Nauru</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Netherland Antilles">Netherland Antilles</option>
                                        <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                        <option value="Nevis">Nevis</option>
                                        <option value="New Caledonia">New Caledonia</option>
                                        <option value="New Zealand">New Zealand</option>
                                        <option value="Nicaragua">Nicaragua</option>
                                        <option value="Niger">Niger</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Niue">Niue</option>
                                        <option value="Norfolk Island">Norfolk Island</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Oman">Oman</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="Palau Island">Palau Island</option>
                                        <option value="Palestine">Palestine</option>
                                        <option value="Panama">Panama</option>
                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                        <option value="Paraguay">Paraguay</option>
                                        <option value="Peru">Peru</option>
                                        <option value="Phillipines">Philippines</option>
                                        <option value="Pitcairn Island">Pitcairn Island</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Portugal">Portugal</option>
                                        <option value="Puerto Rico">Puerto Rico</option>
                                        <option value="Qatar">Qatar</option>
                                        <option value="Republic of Montenegro">Republic of Montenegro</option>
                                        <option value="Republic of Serbia">Republic of Serbia</option>
                                        <option value="Reunion">Reunion</option>
                                        <option value="Romania">Romania</option>
                                        <option value="Russia">Russia</option>
                                        <option value="Rwanda">Rwanda</option>
                                        <option value="St Barthelemy">St Barthelemy</option>
                                        <option value="St Eustatius">St Eustatius</option>
                                        <option value="St Helena">St Helena</option>
                                        <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                        <option value="St Lucia">St Lucia</option>
                                        <option value="St Maarten">St Maarten</option>
                                        <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                                        <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                                        <option value="Saipan">Saipan</option>
                                        <option value="Samoa">Samoa</option>
                                        <option value="Samoa American">Samoa American</option>
                                        <option value="San Marino">San Marino</option>
                                        <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                        <option value="Senegal">Senegal</option>
                                        <option value="Serbia">Serbia</option>
                                        <option value="Seychelles">Seychelles</option>
                                        <option value="Sierra Leone">Sierra Leone</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Slovakia">Slovakia</option>
                                        <option value="Slovenia">Slovenia</option>
                                        <option value="Solomon Islands">Solomon Islands</option>
                                        <option value="Somalia">Somalia</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="Spain">Spain</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="Sudan">Sudan</option>
                                        <option value="Suriname">Suriname</option>
                                        <option value="Swaziland">Swaziland</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Switzerland">Switzerland</option>
                                        <option value="Syria">Syria</option>
                                        <option value="Tahiti">Tahiti</option>
                                        <option value="Taiwan">Taiwan</option>
                                        <option value="Tajikistan">Tajikistan</option>
                                        <option value="Tanzania">Tanzania</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Togo">Togo</option>
                                        <option value="Tokelau">Tokelau</option>
                                        <option value="Tonga">Tonga</option>
                                        <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                                        <option value="Tunisia">Tunisia</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Turkmenistan">Turkmenistan</option>
                                        <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                                        <option value="Tuvalu">Tuvalu</option>
                                        <option value="Uganda">Uganda</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Erimates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States of America">United States of America</option>
                                        <option value="Uraguay">Uruguay</option>
                                        <option value="Uzbekistan">Uzbekistan</option>
                                        <option value="Vanuatu">Vanuatu</option>
                                        <option value="Vatican City State">Vatican City State</option>
                                        <option value="Venezuela">Venezuela</option>
                                        <option value="Vietnam">Vietnam</option>
                                        <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                        <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                        <option value="Wake Island">Wake Island</option>
                                        <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Zaire">Zaire</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabwe">Zimbabwe</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="desc" class="col-sm-3 control-label">Introduction</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="desc" name="description" rows="4" cols="15" ></textarea>
                                </div>
                            </div>

                            <div id="imageDiv" class="form-group">
                                <label for="icon" class="col-sm-3 control-label"></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="icon" name="icon"/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        Save & Continue
                                    </button>
                                    <button type="reset" class="btn btn-default btn-sm">
                                        Reset
                                    </button>
                                </div>
                            </div>

                        </form>
                        <button class="close">Close</button>
                    </div>
                </div>
                <!--Creates the popup body-->
                <button id="addUser" class="open">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AddUser&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
</div>

<!-- RemoveUser -->

<!-- ManagePost -->
<button class="accordion">Manage Articles</button>
<div class="panel">
    <!-- Table -->
    <table id="articleTable" class="table">
        <tr>
            <th>Article ID</th>
            <th>Title</th>
            <th>Date Created</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>

        <tr>
            <!-- To get UserID from database -->
            <td>Article ID</td>
            <!-- To get PostID from database -->
            <td>Title</td>
            <!-- To get DateCreated from database -->
            <td>DateCreated</td>
            <!-- To show whether post is hide or show -->
            <td>Status</td>
            <td>
                <button type="button" class="show">Show</button>
                <button type="button" class="dissapear">Hide</button>
            </td>
        </tr>
    </table>
</div>
<!-- ManagePost -->

<!-- ManageComment -->
<button class="accordion">Manage Comments</button>
<div class="panel">
    <!-- Table -->
    <table id="commentTable" class="table table-striped table-bordered">
        <tr>
            <th>Comment ID</th>
            <th>Content</th>
            <th>Date Created</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>

    </table>
</div>
</div>

    <script>
        $( document ).ready(function () {

            /** Generate User Table  **/
        getAllUsers(50, 0,
            function (data) {
                var users = data["result"];

                for (var i = 0; i < users.length; ++i) {

                var userTableTr = $("<tr></tr>");
                userTableTr.attr("userTableTrId", users[i].id);

                var userTableTd_userId = $("<td></td>");
                userTableTd_userId.append(users[i].id);

                var userTableTd_userName = $("<td></td>");
                userTableTd_userName.append(users[i].username);

                var userTableTd_dob = $("<td></td>");
                userTableTd_dob.append(users[i].dob);

                var userTableTd_country = $("<td></td>");
                userTableTd_country.append(users[i].country);

                var userTableTd_action = $("<td></td>");
                /**var addUserButton = $("<button></button>").text("Add User");
                addUserButton.attr("addUserButtonId", users[i].id);
                addUserButton.attr("class", "open");**/


                var removeUserButton = $("<button></button>").text("Remove User");
                removeUserButton.attr("removeUserButtonId", users[i].id);
                removeUserButton.attr("class", "open");
                removeUserButton.on("click", removeUser);


                var resetUserPwButton = $("<button></button>").text("Reset Password");
                resetUserPwButton.attr("resetUserPwButtonId", users[i].id);
                resetUserPwButton.click(function () {
                        var url = "SendMailForResetPasswordServlet?UserId=" + $(this).attr("resetUserPwButtonId");
                        $.get(
                            {
                                url: url,
                                success: function () {
                                    alertify.alert("a mail sent to the user!", function () {
                                        alertify.message('OK');
                                    }).setHeader('<em> Team Ocelot </em> ');
                                }
                            }
                        );
                });

                userTableTd_action.append(/*addUserButton,*/ removeUserButton, resetUserPwButton);


                userTableTr.append(userTableTd_userId, userTableTd_userName, userTableTd_dob, userTableTd_country, userTableTd_action);

                $("#userTable").append(userTableTr);

                }


            },
            function (data) {
                alertify.alert("Error\n" + data.responseText, function () {
                    alertify.message('OK');
                }).setHeader('<em> Team Ocelot </em> ');
            }
        );


            /** Generate Article Table  **/
            getLastestArticles(50, 0, null,
                function (data) {
                    var articles = data["result"];
                    for (var i = 0; i < articles.length; ++i) {

                        var articleTableTr = $("<tr></tr>");
                        articleTableTr.attr("articleTableTrId", articles[i].id);

                        var articleTableTd_articleId = $("<td></td>");
                        articleTableTd_articleId.append(articles[i].id);

                        var articleTableTd_articleTitle = $("<td></td>");
                        articleTableTd_articleTitle.append(articles[i].title);

                        var articleTableTd_articleDate = $("<td></td>");
                        articleTableTd_articleDate.append(articles[i].pubtime);

                        var articleTableTd_articleStatus = $("<td></td>");
                        var banned = articles[i].banned ? "banned": "normal";
                        articleTableTd_articleStatus.append("" + banned);

                        var articleTableTd_action = $("<td></td>");
                        var showArticleButton = $("<button></button>").text("Show");
                        showArticleButton.attr("showArticleButtonId", articles[i].id);
                        showArticleButton.on("click", showArticle);

                        var hideArticleButton = $("<button></button>").text("Hide");
                        hideArticleButton.attr("hideArticleButtonId", articles[i].id);
                        hideArticleButton.on("click", hideArticle);

                        articleTableTd_action.append(showArticleButton, hideArticleButton);
                        articleTableTr.append(articleTableTd_articleId, articleTableTd_articleTitle, articleTableTd_articleDate, articleTableTd_articleStatus, articleTableTd_action);

                        $("#articleTable").append(articleTableTr);


                    }

                },

                function (data) {
                    alertify.alert("Error\n" + data.responseText, function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                }, false

            );

            /** Generate Comment Table  **/
            getLatestComments(100,0,
                function (data) {
                    var comments = data["result"];
                    for (var i = 0; i < comments.length; ++i) {
                        var commentTableTr = $("<tr></tr>");
                        commentTableTr.attr("commentTableTrId", comments[i].id);

                        var commentTableTd_commentId = $("<td></td>");
                        commentTableTd_commentId.append(comments[i].id);

                        var commentTableTd_commentContent = $("<td></td>");
                        commentTableTd_commentContent.append(comments[i].content);

                        var commentTableTd_commentDate = $("<td></td>");
                        commentTableTd_commentDate.append(comments[i].pubtime);

                        var commentTableTd_commentStatus = $("<td></td>");
                        var banned = comments[i].banned ? "banned": "normal";
                        commentTableTd_commentStatus.append(""+ banned);

                        var commentTableTd_action = $("<td></td>");
                        var showCommentButton = $("<button></button>").text("Show");
                        showCommentButton.attr("showCommentButtonId", comments[i].id);
                        showCommentButton.on("click", showComment);

                        var hideCommentButton = $("<button></button>").text("Hide");
                        hideCommentButton.attr("hideCommentButtonId", comments[i].id);
                        hideCommentButton.on("click", hideComment);

                        commentTableTd_action.append(showCommentButton, hideCommentButton);
                        commentTableTr.append(commentTableTd_commentId, commentTableTd_commentContent, commentTableTd_commentDate, commentTableTd_commentStatus, commentTableTd_action);

                        $("#commentTable").append(commentTableTr);
                    }

                },
                function (data) {
                    alertify.alert("Error\n" + data.responseText, function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                }

            );


        });


        function removeUser() {
            var userId = $(this).attr("removeUserButtonId");

            deleteUserById(userId,
                function (data) {
                    alertify.confirm('Remove user success!', function(){  window.location.replace("Admin.jsp"); }).setHeader('<em> Team Ocelot </em> ');
                },

                function (data) {
                    alertify.alert("Error\n" + data.responseText, function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                }
            )
        }

        function hideComment(){
            var commentId = $(this).attr("hideCommentButtonId");

            setCommentBanned(commentId, true,

                function (data) {
                    alertify.confirm('Hide comment success!', function(){  window.location.replace("Admin.jsp"); }).setHeader('<em> Team Ocelot </em> ');

                },

                function (data) {
                    alertify.confirm("Error\n" + data.responseText, function(){ window.location.replace("Admin.jsp"); }).setHeader('<em> Team Ocelot </em> ');

                })
        }

        function showComment(){
            var commentId = $(this).attr("showCommentButtonId");

            setCommentBanned(commentId, false,

                function (data) {
                    alertify.confirm('Display comment success!', function(){  window.location.replace("Admin.jsp"); }).setHeader('<em> Team Ocelot </em> ');
                },

                function (data) {
                    alertify.alert("Error\n" + data.responseText, function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                })
        }

        function hideArticle(){
            var articleId = $(this).attr("hideArticleButtonId");

            setArticleBanned(articleId, true,

                function (data) {
                    alertify.confirm('Hide article success!', function(){  window.location.replace("Admin.jsp"); }).setHeader('<em> Team Ocelot </em> ');
                },

                function (data) {
                    alertify.alert("Error\n" + data.responseText, function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                })
        }

        function showArticle(){
            var articleId = $(this).attr("showArticleButtonId");

            setArticleBanned(articleId, false,

                function (data) {
                    alertify.confirm('Show article success!', function(){   window.location.replace("Admin.jsp"); }).setHeader('<em> Team Ocelot </em> ');
                },

                function (data) {
                    alertify.alert("Error\n" + data.responseText, function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                })
        }



    </script>

<!-- ManageComment -->

<!-- Logout -->
<button class="accordion">Logout</button>
<div class="panel">
<button class="logout">Logout</button>
</div>
<!-- Logout -->


<script>
    $( document ).ready(function () {
        var acc = document.getElementsByClassName("accordion");
        var i;

        // collapse all panels on load
        $(".panel").hide();

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;

                // reveal the panel
                $(panel).slideToggle();

                $(panel).siblings().each(function() {
                    // remove active style from other elements
                    $(this).removeClass("active");

                    // hide other panels
                    if ($(this).hasClass("panel")) {
                        $(this).slideUp();
                    }
                })
                $(panel).previousElementSibling().addClass("active");
            });

        }
        $("#country").val("${userSession.country}");
        $("#gender").val("${userSession.gender}");


        $("#editForm").submit(function(event) {

            /* stop form from submitting normally */
            event.preventDefault();

            /* get the action attribute from the <form action=""> element */
//            var $form = $( this );
//            var url = $form.attr( 'action' );
//
//            /* Send the data using post with element id name and name2*/
//            var posting = $.post( url, { name: $('#name').val(), name2: $('#name2').val() } );
//
//            /* Alerts the results */
//            posting.done(function( data ) {
//                alert('success');
//            });

            var user = {
                fname : $("#fname").val(),
                username: $("#username").val(),
                lname : $("#lname").val(),
                passwd : $("#passwd").val(),
                gender : $("#gender").val(),
                email : $("#email").val(),
                //icon : $("#icon").val(),
                icon : "Photos/default.png",
                dob : $("#dob").val(),
                country: $("#country").val(),
                description: $("#desc").val()
            };

            addUser(user,
                function (data) {
                    alertify.confirm("New user " + $("#username").val() + " created", function(){  window.location.replace("Admin.jsp"); }).setHeader('<em> Team Ocelot </em> ');
                },

                function (data) {
                    alertify.alert("Add new user failed " + JSON.stringify(data), function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                }
            )


        });

        $('#datePicker')
            .datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'

            })

        //appends an "active" class to .popup and .popup-content when the "Open" button is clicked
        $(".open").on("click", function(){
            $(".popup-overlay, .popup-content").addClass("active").show(".blur");
        });

//removes the "active" class to .popup and .popup-content when the "Close" button is clicked
        $(".close").on("click", function(){
            $(".popup-overlay, .popup-content").removeClass("active");
        });

    })

    /* attach a submit handler to the form */




</script>
</div>

</body>
</html>
