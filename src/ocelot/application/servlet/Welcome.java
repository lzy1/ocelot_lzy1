package ocelot.application.servlet;

import ocelot.application.beans.User;
import ocelot.application.beans.UserSession;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Welcome extends CommonServlet {



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        User userSession = (User) session.getAttribute("userSession");

        if (userSession != null) {
            req.getRequestDispatcher("Profile.jsp").forward(req, resp);
        } else {
            List<String> list = getDefaultAvtarList();

            req.getSession().setAttribute("icons", list);

            req.getRequestDispatcher("Menu.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
