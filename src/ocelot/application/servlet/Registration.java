package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.beans.User;
import ocelot.application.VerifyRecaptcha;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Properties;

public class Registration extends CommonServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.sendRedirect("Menu.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();

        try(DBConnection connection = createDBConnection()) {

            User user = new User();

            user.setUserName(req.getParameter("username"));
            user.setFname(req.getParameter("fname"));
            user.setLname(req.getParameter("lname"));
            user.setPasswd(req.getParameter("passwd"));
            user.setGender(req.getParameter("gender"));
            user.setEmail(req.getParameter("email"));
            user.setIconName(req.getParameter("icon"));
            user.setDob(req.getParameter("dob"));
            user.setCountry(req.getParameter("country"));
            user.setDescription(req.getParameter("description"));

            System.out.println(req.getParameter("dob"));

            if (user.getIconName() == null) {
                user.setIconName("default.png");
            }


            //TODO send an verification email


            String gRecaptchaResponse = req.getParameter("g-recaptcha-response");
            System.out.println("Google: " + gRecaptchaResponse);
            boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);

            if (verify & !user.getCountry().equals("")) {
                connection.addUser(user);
                resp.sendRedirect("Success.html");
            } else if(user.getCountry().equals("")){
                doFail(req, resp, "<font color=red>You need to select you country.</font>");
            }else {
                doFail(req, resp, "<font color=red>You missed the Captcha.</font>");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            showError(req, resp, 400, e.getMessage());

        }

    }

    protected void doFail(HttpServletRequest req,
                          HttpServletResponse resp, String error) throws IOException, ServletException {

        //PrintWriter out= resp.getWriter();
        //out.println("<font color=red>Either user name or password is wrong. Please try again!</font>");
        req.setAttribute("fail", new Boolean(true));
        req.setAttribute("failReason", error);

        req.getRequestDispatcher("Menu.jsp").forward(req, resp);

    }


}