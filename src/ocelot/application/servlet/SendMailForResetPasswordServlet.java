package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.MailSender;
import ocelot.application.Reset;
import ocelot.application.ResetPwdSessionManager;
import ocelot.application.beans.User;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;

import java.util.UUID;


public class SendMailForResetPasswordServlet extends CommonServlet {

    @Override
    public void destroy() {
        ResetPwdSessionManager.instance().stop();
        super.destroy();
    }

    @Override
    public void init() throws ServletException {
        super.init();
        ResetPwdSessionManager.instance().start();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        User admin = (User) req.getSession().getAttribute("userSession");
        String userId = req.getParameter("UserId");
        if (admin != null && admin.getUserName().equals("admin") && userId != null) {

            User user = null;
            try (DBConnection connection = createDBConnection()){
                user = connection.getUserById(userId);

                if (user == null) {
                    sendErrorPage(req, resp, 400, "Can't not find this user!");
                    return;
                }
            } catch (SQLException e) {
                sendErrorPage(req, resp, 400, e.getMessage());
                return;
            } catch (Exception e) {
                sendErrorPage(req, resp, 400, e.getMessage());
                return;
            }


            MailSender sender = createMailSender();


            String url = req.getRequestURL().toString();
            url = url.substring(0, url.lastIndexOf('/'));
            //url += resp.encodeRedirectURL("ChangePasswdServlet");
            url += "/ChangePasswdServlet";
            //url = resp.encodeURL(url);
            //url += resp.encodeRedirectURL("ResetPassword.jsp");

            String index = UUID.randomUUID().toString();

            Instant deadline = Instant.now().plus(Duration.ofMinutes(10));

            ResetPwdSessionManager.instance().putReset(index, new Reset(user.getId(), deadline));

            url += "?index=" + index;
            System.out.println("url:" + url);

            String content = "<h2>Hello</h2><a href=\""+ url +"\">" + "Reset your password for Ocelot account" +"</a>";
            System.out.println(content);

            sender.sendMail(user.getEmail(), "Reset password for Ocelot Blogger", content);

            resp.setStatus(200);
            out.println("<html>\n<head><title>Send Email Succeed</title>");
            out.println("</head>\n<body>");
            out.println("<h1>An email has been sent to email address "+ user.getEmail() + "</h1>");
            out.println("</body></html>");
        } else {

            resp.setStatus(400);
            out.println("<html>\n<head><title>Send Email Failed</title>");
            out.println("</head>\n<body>");
            out.println("<h1>You are not authorized to change other's password!</h1>");
            out.println("</body></html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

}
