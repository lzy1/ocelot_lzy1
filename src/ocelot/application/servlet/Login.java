package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.MailSender;
import ocelot.application.VerifyRecaptcha;
import ocelot.application.beans.User;
import ocelot.application.beans.UserSession;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Login extends CommonServlet {
    private static final long serialVersionUID = 1L;


    protected void doPost(HttpServletRequest req,
                          HttpServletResponse resp) throws ServletException, IOException {

        // get req parameters for userID and password
        String userP = req.getParameter("username");
        String pwdP = req.getParameter("passwd");
        //String gRecaptchaResponse = req.getParameter("g-recaptcha-response");
        //System.out.println("Google: " + gRecaptchaResponse);
        System.out.println(userP + " " + pwdP);

        //boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);

        try(DBConnection connection = createDBConnection()) {

            User user = connection.getUserByUserName(req.getParameter("username"));

            if (user!= null){

                String userID = "";
                String password = "";
                String userName = "";

                userID = user.getId();
                userName = user.getUserName();
                password = user.getPasswd();

                if (userP.equals(userName) && pwdP.equals(password) /*&& verify*/){
                    HttpSession session = req.getSession();
                    session.setAttribute("userSession", user);

                    //setting session to expiry in 30 mins
                    session.setMaxInactiveInterval(30*60);

                    if(userName.equals("admin") & password.equals("admin")){
                        req.getRequestDispatcher("Admin.jsp").forward(req, resp);
                    } else {
                        req.getRequestDispatcher("Profile.jsp").forward(req, resp);
                    }

                    //MailSender sender = createMailSender();
                    //sender.sendMail("tangyiyi008@gmail.com", "Ocelot Login", userName + " just logined!");

                } else {
                    doFail(req, resp, "<font color=red>Either user name or password is wrong. Please try again!</font>");
                }
            } else {
                    doFail(req, resp, "<font color=red>Either user name or password is wrong. Please try again!</font>");
            }


             } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doFail(HttpServletRequest req,
                          HttpServletResponse resp, String error) throws IOException, ServletException {

        //PrintWriter out= resp.getWriter();
        //out.println("<font color=red>Either user name or password is wrong. Please try again!</font>");
        req.setAttribute("fail", new Boolean(true));
        req.setAttribute("failReason", error);

        req.getRequestDispatcher("Menu.jsp").forward(req, resp);

    }
}
