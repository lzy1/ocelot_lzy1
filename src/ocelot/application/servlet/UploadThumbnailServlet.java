package ocelot.application.servlet;

import ocelot.application.beans.User;
import org.apache.commons.fileupload.FileItem;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class UploadThumbnailServlet extends UploadServlet {

    public String getPrefix(String filename) {
        return filename.substring(0, filename.lastIndexOf('.'));
    }

    public String getSuffix(String filename) {
        return filename.substring(filename.lastIndexOf('.'), filename.length());
    }

    public String generateFileName(String path) {
        File file = new File(path);
        String name = path;
        String suffix = getSuffix(path);

        int i = 1;
        while (file.exists()) {
            name = getPrefix(path) + i + suffix;
            file = new File(name);
            i++;
        }

        return name;
    }

    @Override
    protected String onUploadFile(User user, FileItem fi) {
        //String fieldName = fi.getFieldName();
        String fileName = fi.getName();
        String contentType = fi.getContentType();
        boolean isInMemory = fi.isInMemory();
        long sizeInBytes = fi.getSize();

        //System.out.println("fieldName:" + fieldName);
        System.out.println("fileName:" + fileName);


        File photodir = new File(getUserPhotoDir(user));

        if (!photodir.exists()) {
            photodir.mkdirs();
        }

        String orgFileName = generateFileName(getUserPhotoDir(user)
                + File.separator + fileName);

        File imgFile = new File(orgFileName);
        try {
            fi.write(imgFile);
            System.out.println("create image: " + imgFile.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        File thumbDir = new File(getUserThumbNailPath(user));

        if (!thumbDir.exists()) {
            thumbDir.mkdirs();
        }

        String thumbFileName = "thumbnail_" + fileName.substring(0, fileName.lastIndexOf('.')) + ".png";

        String realName = generateFileName(getUserThumbNailPath(user) + File.separator + thumbFileName);
        File thuFile = new File(realName);


        createThumbnail(imgFile, realName, 128, 128);
        String thumbNameUrl = getUserRelativeThumbnailDirUrl(user) + "/" + thuFile.getName();

        return thumbNameUrl;

    }

    private void createThumbnail(File file, String outputfile, int width, int height) {
        System.out.println("createThumbnail: " + outputfile);
        BufferedImage before = null;
        try {
            before = ImageIO.read(file);
            int w = before.getWidth();
            int h = before.getHeight();

            Image toolkitImage = before.getScaledInstance(width, height,
                    Image.SCALE_SMOOTH);

            // width and height are of the toolkit image
            BufferedImage after = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_ARGB);
            Graphics g = after.getGraphics();
            g.drawImage(toolkitImage, 0, 0, null);
            g.dispose();

            File outfile = new File(outputfile);
            ImageIO.write(after, "png", outfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
